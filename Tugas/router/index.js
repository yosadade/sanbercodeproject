import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import AboutScreen from '../Tugas13/AboutScreen';
import LoginScreen from '../Tugas13/LoginScreen';
import ProfileScreen from '../Tugas13/ProfileScreen';
import {NavigationContainer} from '@react-navigation/native';

const Stack = createStackNavigator();

const Router = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="HomeScreen"
        screenOptions={{
          headerShown: false,
          ...TransitionPresets.SlideFromRightIOS,
        }}>
        <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="AboutScreen" component={AboutScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Router;
