import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import data from './skillData.json';

const SkillScreen = ({navigation}) => {
  return (
    <View style={styles.page}>
      <RenderStatusBar />
      <RenderHeader navigation={navigation} />
      <RenderContent />
    </View>
  );
};

const RenderStatusBar = () => {
  return (
    <StatusBar
      barStyle="dark-content"
      backgroundColor="#FFFFFF"
      hidden={false}
    />
  );
};

const RenderHeader = ({navigation}) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity>
        <MaterialIcons name="arrow-back" size={22} />
      </TouchableOpacity>
      <Text style={styles.titleHeader}>Profile</Text>
      <View style={styles.gap} />
    </View>
  );
};

const RenderContent = () => {
  return (
    <View style={styles.content}>
      <RenderProgrammingLang />
      <RenderFramework />
      <RenderTeknologi />
    </View>
  );
};

const RenderProgrammingLang = () => {
  return (
    <View>
      <Text style={styles.label}>Programming Language</Text>
      <FlatList
        data={data.programmingLang}
        renderItem={({item}) => {
          return (
            <View style={styles.skills}>
              <MaterialCommunityIcons
                name={item.iconName}
                size={item.size}
                color={item.color}
              />
              <View style={styles.percentaseWrapper}>
                <Text style={styles.title}>{item.skillName}</Text>
                <View style={styles.percentase}>
                  <View style={styles.percentaseBlue} />
                  <View style={styles.percentaseGrey} />
                  <View style={styles.wrapperTitlePercentase}>
                    <Text style={styles.title}>{item.percentageProgress}</Text>
                  </View>
                </View>
              </View>
            </View>
          );
        }}
        keyExtractor={(item, index) => item + index.toString()}
      />
    </View>
  );
};

const RenderFramework = () => {
  return (
    <View style={styles.wrapperLabel}>
      <Text style={styles.label}>Framework Library</Text>
      <FlatList
        data={data.framework}
        renderItem={({item}) => {
          return (
            <View style={styles.skills}>
              <MaterialCommunityIcons
                name={item.iconName}
                size={item.size}
                color={item.color}
              />
              <View style={styles.percentaseWrapper}>
                <Text style={styles.title}>{item.skillName}</Text>
                <View style={styles.percentase}>
                  <View style={styles.percentaseBlue} />
                  <View style={styles.percentaseGrey} />
                  <View style={styles.wrapperTitlePercentase}>
                    <Text style={styles.title}>{item.percentageProgress}</Text>
                  </View>
                </View>
              </View>
            </View>
          );
        }}
        keyExtractor={(item, index) => item + index.toString()}
      />
    </View>
  );
};

const RenderTeknologi = () => {
  return (
    <View style={styles.wrapperLabel}>
      <Text style={styles.label}>Teknologi</Text>
      <FlatList
        data={data.teknologi}
        renderItem={({item}) => {
          return (
            <View style={styles.skills}>
              <MaterialCommunityIcons
                name={item.iconName}
                size={item.size}
                color={item.color}
              />
              <View style={styles.percentaseWrapper}>
                <Text style={styles.title}>{item.skillName}</Text>
                <View style={styles.percentase}>
                  <View style={styles.percentaseBlue} />
                  <View style={styles.percentaseGrey} />
                  <View style={styles.wrapperTitlePercentase}>
                    <Text style={styles.title}>{item.percentageProgress}</Text>
                  </View>
                </View>
              </View>
            </View>
          );
        }}
        keyExtractor={(item, index) => item + index.toString()}
      />
    </View>
  );
};

export default SkillScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 40,
    paddingVertical: 20,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleHeader: {
    flex: 1,
    fontSize: 20,
    fontWeight: '800',
    textAlign: 'center',
  },
  content: {
    paddingVertical: 40,
  },
  wrapperLabel: {
    paddingVertical: 20,
  },
  label: {
    fontSize: 16,
    color: '#7D8797',
    marginBottom: 6,
    fontFamily: '400',
  },
  skills: {
    flexDirection: 'row',
    paddingTop: 10,
    alignItems: 'center',
  },
  title: {
    fontSize: 14,
    color: '#7D8797',
    fontFamily: '400',
  },
  percentaseWrapper: {
    paddingLeft: 10,
  },
  percentase: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  percentaseBlue: {
    backgroundColor: '#0BCAD4',
    width: 140,
    height: 2,
  },
  percentaseGrey: {
    backgroundColor: '#7D8797',
    width: 40,
    height: 2,
  },
  percentaseBlue80: {
    backgroundColor: '#0BCAD4',
    width: 150,
    height: 2,
  },
  percentaseGrey80: {
    backgroundColor: '#7D8797',
    width: 30,
    height: 2,
  },
  percentaseBlue60: {
    backgroundColor: '#0BCAD4',
    width: 100,
    height: 2,
  },
  percentaseGrey60: {
    backgroundColor: '#7D8797',
    width: 80,
    height: 2,
  },
  wrapperTitlePercentase: {
    paddingLeft: 10,
  },
});
