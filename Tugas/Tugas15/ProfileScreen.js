import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  StatusBar,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const ProfileScreen = ({navigation}) => {
  return (
    <View style={styles.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <RenderStatusBar />
        <RenderHeader navigation={navigation} />
        <RenderAvatar />
        <RenderContent navigation={navigation} />
      </ScrollView>
    </View>
  );
};

const RenderStatusBar = () => {
  return (
    <StatusBar
      barStyle="dark-content"
      backgroundColor="#FFFFFF"
      hidden={false}
    />
  );
};

const RenderHeader = ({navigation}) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <MaterialIcons name="arrow-back" size={22} />
      </TouchableOpacity>
      <Text style={styles.titleHeader}>Profile</Text>
      <View style={styles.gap} />
    </View>
  );
};

const RenderAvatar = () => {
  return (
    <View style={styles.avatar}>
      <Image source={require('./images/profile.png')} style={styles.img} />
      <Text style={styles.name}>Yosada Dede Arissa</Text>
      <Text style={styles.profession}>Mobile Developer</Text>
      <View style={styles.gap} />
    </View>
  );
};

const RenderContent = ({navigation}) => {
  return (
    <View style={styles.content}>
      <View style={styles.wrapperInput}>
        <Text style={styles.label}>Facebook</Text>
        <Text style={styles.title}>yozz.dee</Text>
      </View>
      <View style={styles.wrapperInput}>
        <Text style={styles.label}>Linkedin</Text>
        <Text style={styles.title}>@yosadade</Text>
      </View>
      <View style={styles.wrapperInput}>
        <Text style={styles.label}>Github</Text>
        <Text style={styles.title}>github.com/yosadade</Text>
      </View>
      <View style={styles.wrapperInput}>
        <Text style={styles.label}>Twitter</Text>
        <Text style={styles.title}>@yosadade</Text>
      </View>
      <View style={styles.wrapperInput}>
        <Text style={styles.label}>Instagram</Text>
        <Text style={styles.title}>@yosadade</Text>
      </View>
      <View style={styles.gap} />
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('AboutScreen')}>
        <Text style={styles.titleButton}>Continue</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 40,
    paddingVertical: 20,
    justifyContent: 'space-around',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleHeader: {
    flex: 1,
    fontSize: 20,
    fontWeight: '800',
    textAlign: 'center',
  },
  avatar: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  name: {
    fontSize: 20,
    color: '#000000',
    fontWeight: '800',
  },
  profession: {
    fontSize: 16,
    color: '#7D8797',
    fontWeight: '600',
  },
  content: {},
  img: {
    marginVertical: 20,
    width: 130,
    height: 130,
    borderRadius: 130,
  },
  wrapperInput: {
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderColor: '#E9E9E9',
  },
  label: {
    fontSize: 16,
    color: '#7D8797',
    marginBottom: 6,
    fontFamily: '400',
  },
  title: {
    fontSize: 14,
    color: '#112340',
    marginBottom: 6,
    fontFamily: '400',
  },
  input: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#E9E9E9',
    padding: 12,
  },
  gap: {
    height: 40,
    width: 20,
  },
  button: {
    paddingVertical: 15,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0BCAD4',
  },
  titleButton: {
    fontSize: 18,
    color: '#FFFFFF',
    fontWeight: '600',
  },
});
