import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';

import AboutScreen from '../Tugas15/AboutScreen';
import LoginScreen from '../Tugas15/LoginScreen';
import ProfileScreen from '../Tugas15/ProfileScreen';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const MyDrawer = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="MainApp" component={MainApp} />
      <Drawer.Screen name="Profile" component={ProfileScreen} />
    </Drawer.Navigator>
  );
};

const MyStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="LoginScreen"
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.SlideFromRightIOS,
      }}>
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name="MyDrawer" component={MyDrawer} />
    </Stack.Navigator>
  );
};

const MainApp = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: '#0BCAD4',
        style: {
          height: 60,
          backgroundColor: '#FFFFFF',
        },
        labelStyle: {
          fontSize: 12,
          marginBottom: 10,
          fontWeight: 'bold',
        },
        tabStyle: {
          marginTop: 10,
        },
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          if (route.name === 'Project') {
            return (
              <MaterialCommunityIcons
                name="code-greater-than-or-equal"
                size={size}
                color={color}
              />
            );
          } else if (route.name === 'Add') {
            return <Ionicons name="ios-add-circle" size={size} color={color} />;
          } else if (route.name === 'About') {
            return <Entypo name="code" size={size} color={color} />;
          }
        },
      })}>
      <Tab.Screen name="Project" component={ProjectScreen} />
      <Tab.Screen name="Add" component={AddScreen} />
      <Tab.Screen name="About" component={AboutScreen} />
    </Tab.Navigator>
  );
};

const Router = () => {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
};

export default Router;
