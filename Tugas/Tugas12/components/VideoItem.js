import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const videoItem = ({video}) => {
  const {url} = video.snippet.thumbnails.medium;
  const {title, channelTitle} = video.snippet;
  return (
    <View style={styles.container}>
      <Image source={{uri: url}} style={styles.img} />
      <View style={styles.descContainer}>
        <Image source={{uri: url}} style={styles.imgProfile} />
        <View style={styles.videoDetails}>
          <Text style={styles.videoTitle}>{title}</Text>
          <Text style={styles.viewStats}>{`${channelTitle} . ${nFormatter(
            video.statistics.viewCount,
            1,
          )} . 3 months ago`}</Text>
        </View>
        <TouchableOpacity>
          <Icon name="more-vert" size={20} color="grey" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const nFormatter = (num, digits) => {
  var si = [
    {value: 1, symbol: ''},
    {value: 1e3, symbol: 'k'},
    {value: 1e6, symbol: 'M'},
    {value: 1e9, symbol: 'G'},
    {value: 1e12, symbol: 'T'},
    {value: 1e15, symbol: 'P'},
    {value: 1e18, symbol: 'E'},
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (
    (num / si[i].value).toFixed(digits).replace(rx, '$1') +
    si[i].symbol +
    ' views'
  );
};

export default videoItem;

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  img: {
    height: 200,
  },
  descContainer: {
    flexDirection: 'row',
    paddingTop: 15,
  },
  imgProfile: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
  },
  videoDetails: {
    paddingHorizontal: 15,
    flex: 1,
  },
  videoTitle: {
    fontSize: 16,
    color: '#3C3C3C',
  },
  viewStats: {
    fontSize: 14,
    paddingTop: 3,
  },
});
