import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/VideoItem';
import data from './data.json';

const App = () => {
  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
        <Image
          source={require('../Tugas12/images/logo.png')}
          style={styles.img}
        />
        <View style={styles.rightNav}>
          <TouchableOpacity>
            <Icon style={styles.navItem} name="search" size={25} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon name="account-circle" size={25} color="grey" />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.body}>
        {/* <VideoItem video={data.items[0]} /> */}
        <FlatList
          data={data.items}
          renderItem={video => <VideoItem video={video.item} />}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
        />
      </View>
      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="home" size={25} color="#3C3C3C" />
          <Text style={styles.tabTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="whatshot" size={25} color="#3C3C3C" />
          <Text style={styles.tabTitle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="subscriptions" size={25} color="#3C3C3C" />
          <Text style={styles.tabTitle}>Subscripts</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="folder" size={25} color="#3C3C3C" />
          <Text style={styles.tabTitle}>Library</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  navBar: {
    paddingHorizontal: 15,
    height: 55,
    backgroundColor: '#FFFFFF',
    elevation: 3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  img: {
    width: 98,
    height: 22,
  },
  rightNav: {
    flexDirection: 'row',
  },
  navItem: {
    marginRight: 25,
  },
  body: {
    flex: 1,
  },
  itemSeparator: {
    height: 0.5,
    backgroundColor: '#CCCCCC',
  },
  tabBar: {
    backgroundColor: '#FFFFFF',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5EF',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabTitle: {
    fontSize: 11,
    color: '#3C3C3C',
    paddingTop: 3,
  },
});
